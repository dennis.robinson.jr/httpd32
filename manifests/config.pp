#
class httpd32::config inherits httpd32
{

  $httpd_file         = "${httpd32::prefix}/conf/httpd.conf"
  $httpd_default_file = "${httpd32::prefix}/conf/extra/httpd-default.conf"
  $httpd_ssl_file     = "${httpd32::prefix}/conf/extra/httpd-ssl.conf"
  $host_name          = $facts['fqdn']

  #############################################
  # CONFIGURE HTTPD SERVICE
  #############################################

  exec { 'httpd-systemd-reload' :
    command     => '/usr/bin/systemctl daemon-reload',
    refreshonly => true,
  }

  file { 'create-httpd-unit-file' :
    path    => '/etc/systemd/system/httpd.service',
    content => template('httpd32/httpd.service.erb'),
    mode    => '0664',
    notify  => Exec['httpd-systemd-reload'],
    before  => File_line['add-httpd-user'],
  }

  file { 'create-httpd-environment-file' :
    ensure  => file,
    path    => '/etc/sysconfig/httpd',
    mode    => '0664',
    notify  => Exec['httpd-systemd-reload'],
    require => File['create-httpd-unit-file']
  }

  #############################################
  # APACHE BASE CONIFG
  #############################################

  file_line { 'add-httpd-user' :
    ensure  => present,
    path    => $httpd_file,
    line    => "User ${httpd32::install_user}",
    match   => '^User',
    require => File[$httpd_file],
  }

  file_line { 'add-httpd-group' :
    ensure  => present,
    path    => $httpd_file,
    line    => "Group ${httpd32::install_group}",
    match   => '^Group',
    require => File[$httpd_file],
  }

  file_line { 'add-httpd-port' :
    ensure  => present,
    path    => $httpd_file,
    line    => "Listen ${httpd32::http_port}",
    match   => '^Listen',
    require => File[$httpd_file],
  }

  file_line { 'disable-options-directive' :
    ensure   => present,
    path     => $httpd_file,
    line     => "\s\s\s\sOptions\s\"None\"",
    match    => '^(?=.*Options)(?!.*#).*',
    multiple => true,
    replace  => true,
    require  => File[$httpd_file],
  }

  file_line { 'enable-default-conf' :
    ensure  => present,
    path    => $httpd_file,
    line    => 'Include conf/extra/httpd-default.conf',
    #match   => '^#Include conf/extra/httpd-default.conf',
    require => File[$httpd_file],
  }

  $httpd32::default_directives.each |$directive, $value| {

    if $value != undef {

      file_line { $directive :
        ensure  => present,
        path    => $httpd_default_file,
        line    => "${directive} ${value}",
        match   => "^${directive} ",
        require => File[$httpd_default_file],
      }

    }

  }

  #############################################
  # APACHE SSL CONIFG
  #############################################

  if $httpd32::ssl_enabled {

    file { $httpd32::passphrase_file :
      content => "#!/bin/sh\necho ${httpd32::keyfile_pwd}",
      mode    => '0755',
      before  => File_line['enable-ssl'],
    }

    file_line { 'enable-ssl' :
      ensure  => present,
      path    => $httpd_file,
      line    => 'Include conf/extra/httpd-ssl.conf',
      match   => 'Include conf/extra/httpd-ssl.conf',
      require => File[$httpd_file],
    }

    file_line {'add-ssl-servername' :
      ensure  => present,
      path    => $httpd_ssl_file,
      line    => "ServerName ${host_name}",
      match   => '^ServerName',
      require => File[$httpd_ssl_file],
    }

    file_line {'add-ssl-port' :
      ensure  => present,
      path    => $httpd_ssl_file,
      line    => "Listen ${httpd32::ssl_port}",
      match   => '^Listen',
      require => File[$httpd_ssl_file],
    }

    file_line {'add-virtualhost-ssl-port' :
      ensure  => present,
      path    => $httpd_ssl_file,
      line    => "<VirtualHost _default_:${httpd32::ssl_port}>",
      match   => '^<VirtualHost _default_',
      require => File[$httpd_ssl_file],
    }

    file_line {'add-ssl-certificate-file' :
      ensure  => present,
      path    => $httpd_ssl_file,
      line    => "SSLCertificateFile ${httpd32::certfile}",
      match   => '^SSLCertificateFile',
      require => File[$httpd_ssl_file],
    }

    file_line {'add-ssl-certificate-key-file' :
      ensure  => present,
      path    => $httpd_ssl_file,
      line    => "SSLCertificateKeyFile ${httpd32::keyfile}",
      match   => '^SSLCertificateKeyFile',
      require => File[$httpd_ssl_file],
    }

    file_line {'add-ssl-ca-certificate-file' :
      ensure  => present,
      path    => $httpd_ssl_file,
      line    => "SSLCACertificateFile ${httpd32::cacertfile}",
      match   => '^SSLCACertificateFile',
      require => File[$httpd_ssl_file],
    }

    file_line { 'add-ssl_random-seed-startup' :
      ensure  => present,
      path    => $httpd_ssl_file,
      line    => 'SSLRandomSeed startup file:/dev/urandom  256',
      match   => 'SSLRandomSeed startup file:.dev.urandom',
      require => File[$httpd_ssl_file],
    }

    file_line { 'add-ssl_random-seed-connect' :
      ensure  => present,
      path    => $httpd_ssl_file,
      line    => 'SSLRandomSeed connect builtin',
      after   => '^SSLRandomSeed startup',
      require => File[$httpd_ssl_file],
    }

    file_line { 'add-ssl_random-seed-connect_absent' :
      ensure => absent,
      path   => $httpd_ssl_file,
      line   => 'SSLRandomSeed connect file:.dev.urandom',
    }

    file_line { 'add-ssl_passphrase-dialog' :
      ensure  => present,
      path    => $httpd_ssl_file,
      line    => "SSLPassPhraseDialog exec:${httpd32::passphrase_file}",
      match   => '^SSLPassPhraseDialog',
      require => File[$httpd_ssl_file],
    }

  }
  else  {

    file_line { 'disable-ssl' :
      ensure  => present,
      path    => $httpd_file,
      line    => '#Include conf/extra/httpd-ssl.conf',
      match   => 'Include conf/extra/httpd-ssl.conf',
      require => File[$httpd_file],
    }

  }

}
