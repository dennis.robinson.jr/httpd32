#
class httpd32::service inherits httpd32
{

  service { 'httpd' :
    ensure  => running,
    enable  => true,
    require => [ File['create-httpd-unit-file'], File['create-httpd-environment-file'] ]
  }

}
