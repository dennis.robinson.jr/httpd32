#
class httpd32::install inherits httpd32
{

  File { ensure => present, owner => $httpd32::install_user, group => $httpd32::install_group, mode => '0755' }
  Exec {
    path      => "/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/puppetlabs/bin:/home/${httpd32::install_user}/bin",
    user      => $httpd32::install_user,
    group     => $httpd32::install_group,
    logoutput => $httpd32::g_logoutput
  }

  #############################################
  # INSTALL UTILS DEFINED TYPES
  #############################################

  utils::directories { "${name}-root-dirs" :
    directories => [$httpd32::download_dir],
    owner       => 'root',
    group       => 'root',
    before      => Exec['extract-apache-source'],
  }

  utils::firewall { $name :
    rules  => $httpd32::firewall,
    before => Exec['extract-apache-source'],
  }

  if $httpd32::change_certs == false {

    utils::certs { $name :
      certsource       => $httpd32::cert_source,
      certfile         => $httpd32::certfile,
      keyfile          => $httpd32::keyfile,
      cacertfile       => $httpd32::cacertfile,
      intermediatefile => $httpd32::intermediatefile,
      before           => Exec['extract-apache-source'],
    }

  }

  #############################################
  # INSTALL APACHE
  #############################################

  $tar                = split($httpd32::install_source, '/')[-1]
  $tar_strip_ext      = split($tar, '.tar.gz')
  $base_name          = $tar_strip_ext[0]
  $build_dir          = "/home/${httpd32::install_user}/apache_build"
  $httpd_file         = "${httpd32::prefix}/conf/httpd.conf"
  $httpd_default_file = "${httpd32::prefix}/conf/extra/httpd-default.conf"
  $httpd_ssl_file     = "${httpd32::prefix}/conf/extra/httpd-ssl.conf"
  $httpd_created      = "${httpd32::exec_prefix}/bin/apachectl"

  if $httpd32::enable_ssl {
    $ssl = '--enable-ssl'
  }
  else {
    $ssl = '--enable-ssl'
  }

  $config_cmd      =  @("EOT"/L)
                      cd ${build_dir}/${base_name} && \
                      ${build_dir}/${base_name}/configure --prefix=${httpd32::prefix} --exec-prefix=${httpd32::exec_prefix} \
                      --host=${httpd32::host} --with-port=${httpd32::http_port} ${ssl} ${httpd32::apache_options}
                      |-EOT
  $extract_cmd     = "/usr/bin/tar xf ${httpd32::install_source} -C ${build_dir}"


  ##################################################
  # CLEANUP APACHE DIRS IF REBUILDING
  ##################################################
  if $httpd32::rebuild_apache {

    $cleanup_cmd = @("EOT"/L)
                   /bin/systemctl stop httpd && /bin/systemctl disable httpd && \
                   /bin/rm -rf ${httpd32::prefix} ${httpd32::exec_prefix} ${build_dir}
                   |-EOT

    exec {'remove-apache-for-rebuild' :
      command => $cleanup_cmd,
      #cwd     => "/home/${httpd32::install_user}/",
      user    => 'root',
      group   => 'root',
      before  => Exec['extract-apache-source'],
    }

  }

  ##################################################
  # SETUP EXTRACT RESOURCES
  ##################################################

  exec {'extract-apache-source' :
    creates => "${build_dir}/${base_name}/configure",
    command => $extract_cmd,
    #cwd     => "/home/${httpd32::install_user}/",
    require => File['apache-build-directory'],
  }

  file {'apache-build-directory' :
    ensure => directory,
    path   => $build_dir,
  }

  ##################################################
  # SETUP CONFIGURE SOURCE TREE RESOURCE
  ##################################################

  if $httpd32::g_logoutput { notify {"configure-source-tree: ${config_cmd} " : }}

  ensure_packages([$httpd32::packages], { ensure => 'present' })

  exec {'configure-source-tree' :
    creates => $httpd_created,
    command => $config_cmd,
    #cwd     => "${build_dir}/${base_name}",
    require => [ Package[$httpd32::packages], Exec['extract-apache-source'] ]
  }

  ##################################################
  # SETUP MAKE RESOURCE
  ##################################################

  exec {'build-apache' :
    creates => $httpd_created,
    command => "cd ${build_dir}/${base_name} && /usr/bin/make",
    #cwd     => "${build_dir}/${base_name}",
    require => Exec['configure-source-tree']
  }

  ##################################################
  # SETUP INSTALL RESOURCE
  ##################################################

  exec {'install-apache' :
    creates => $httpd_created,
    command => "cd ${build_dir}/${base_name} && /usr/bin/make install",
    #cwd     => "${build_dir}/${base_name}",
    user    => 'root',
    group   => 'root',
    require => Exec['build-apache'],
  }

  exec { 'set-apache-log-dir-permission' :
    command => "/usr/bin/chmod -R 777 ${httpd32::prefix}/logs",
    user    => 'root',
    group   => 'root',
    unless  => "/bin/stat --format '%a' ${httpd32::prefix}/logs | grep 777",
    require => Exec['install-apache'],
  }

  exec { 'set-apache-htdocs-dir-permission' :
    command => "/usr/bin/chmod -R 777 ${httpd32::prefix}/htdocs",
    user    => 'root',
    group   => 'root',
    unless  => "/bin/stat --format '%a' ${httpd32::prefix}/htdocs | grep 777",
    require => Exec['install-apache'],
  }

  ##################################################
  # ENSURE CONFIG FILES WERE CREATED FROM INTSALL
  ##################################################

  file { $httpd_file :
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
    before  => Class['httpd32::config'],
    require => Exec['install-apache'],
  }

  file { $httpd_default_file :
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
    before  => Class['httpd32::config'],
    require => Exec['install-apache'],
  }

  file { $httpd_ssl_file :
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
    before  => Class['httpd32::config'],
    require => Exec['install-apache'],
  }

}
