#
class httpd32
(
  # installation settings
  String                            $install_source              = undef,
  String                            $install_group               = undef,
  String                            $install_user                = undef,
  Boolean                           $change_certs                = false,
  Boolean                           $rebuild_apache              = false,
  #directories
  String                            $download_dir                = undef,
  # apache configuration settings
  String                            $prefix                      = undef,
  String                            $exec_prefix                 = undef,
  String                            $host                        = undef,
  String                            $mpm_type                    = undef,
  Integer                           $http_port                   = undef,
  Integer                           $ssl_port                    = undef,
  Boolean                           $ssl_enabled                 = false,
  String                            $passphrase_file             = undef,
  String                            $apache_options              = undef,
  Hash                              $default_directives          = undef,
  # tls settings
  Optional[Boolean]                 $config_tls                  = false,
  Optional[String]                  $cert_source                 = undef,
  Optional[String]                  $cacertfile                  = undef,
  Optional[String]                  $intermediatefile            = undef,
  Optional[String]                  $certfile                    = undef,
  Optional[String]                  $keyfile                     = undef,
  Optional[String]                  $keyfile_pwd                 = undef,
  Optional[Boolean]                 $peer_auth                   = undef,
  Optional[Boolean]                 $peer_cert_val               = undef,
  Optional[String]                  $keystore_pwd                = undef,
  Optional[String]                  $identkeystore               = undef,
  Optional[String]                  $trustkeystore               = undef,
  Optional[String]                  $keystorealias               = undef,
  Optional[Any]                     $trustedcerts                = undef,
  # operating system settings
  Optional[Variant[Hash,String]]    $firewall                    = undef,
  Variant                           $packages                    = undef,
)
{

  contain httpd32::install
  contain httpd32::config
  contain httpd32::service

  Class['httpd32::install'] -> Class['httpd32::config']
  Class['httpd32::config'] ~> Class['httpd32::service']

}
